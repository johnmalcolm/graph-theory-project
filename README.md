![alt text][logo]
[logo]: http://www.irishtimes.com/polopoly_fs/1.2550281.1456513107!/image/image.png_gen/derivatives/box_300/image.png "Credit Irish Times"

# Irish Election 2016 Neo4j Graph DB 
###### John Malcolm Anderson, G00290919

## Introduction
This project is about representing the constituencies, parties, social media and other info of the 2016 Irish election candidates. 

Node.JS was used to call public API's and also scrap the web to build a high speed graph databse in Neo4J so that we can make interesting queries and get usefull insights into key trends and influencers in the 2016 election.

## Database 
## Database Information
####Node labels 
![alt text][labels]
[labels]: http://www.johnmalcolmdesign.com/labels.png "Labels"

####Relationship types
- **FRIENDS:** Between TwitterLeaf(friend) and Twitter node for a specifiec candidate. 
- **IS_ON:** Between candidate and social media account nodes for that specific candidate. 
- **IS_RUNNING_IN:** Between candidate and constituency.

####Property keys
- **Candidate:** id, first_name, last_name, gender, party_profile_url, phone, photo_url, website, constituency
- **Contituency:** Name, area 
- **TwitterLeaf**: followed_by, handle, twitter_id
- **Twitter**: twitter_id, url
- **Facebook:** url
- **LinkedIn:** url

### Files
- **election.grass:** Stylesheet for Neo4J
- **js/candidates.js:** Buildes Candidates, constiuencies and social media nodes; and also also buildes relationship between them. Commented out code was also used to scrap twitter for data using a bash cron job. (Now moved to a different file for clarity)
- **js/createTwitterLeafs.js:** Used to build the Twitter Leaf nodes from the twitter data files. 
- **js/mergeDuplicateNodes.js:** Used to try and merge duplicate nodes (not yet working correctly).
- **js/package.json:** NPM file for downloading and managing project dependencies. 
- **js/social.js:** Simple twitter api caller to check my rate limit status - used for cron job.
- **js/rate-limit.json**: Result from social.js rate-limit checker.
- **js/twitcount.js**: Simple counter used by cron job for scraping the twitter API.
- **js/data:**: Data results of twitter api scraper seperated into 17 sub folders. 

### Building the database


## Issues
- **Twitter rate limit**
- **Data Cleansing**
- **Neo4J HTTP REST Api**
- **Node.JS internal memory**


## Queries
My queries are mostly related to social media information and building the database on the fly via public API's.
The second query can be run on the database, the two others are used within node to create query string to send to the Neo REST server to build the db on the fly.

#### Query one title
This query uses variables to build a relationship between two pre existing nodes via there first and last names.
```cypher
MATCH (candidate:Candidate { first_name : '" +firstName+ "', last_name : '" +lastName+ "'}), ("+network+":"+network+"{url : '" +urlClean+ "'}) 
CREATE (candidate)-[r:IS_ON]->("+network+")"
```

#### Query two title
This query is about finding what candidates are following a certain twitter user via a Twitter ID. I have limited the result to 10 for speed.
```cypher
MATCH (c:Candidate)-[t:IS_ON]->(v:Twitter)-[r:friends]->(n:TwitterLeaf) 
WHERE n.twitter_id = 79365337 RETURN c, r LIMIT 10;
```

#### Query three title
This query uses the UNWIND to transform a collection back into individual rows and is used for building the TwitterLeaf nodes.
This was the only acceptable query that worked for me due to the very large volume of data. Sending individual requests per node crashed the server every time or crashed Node due to storage and garbage collection issues.
```cypher
UNWIND { props } AS map CREATE (n:TwitterLeaf) SET n = map

```

## References
1. [Neo4J website](http://neo4j.com/), the website of the Neo4j database.
2. http://stackoverflow.com/questions/25315926/merge-existing-records-in-neo4j-remove-duplicates-keep-relationships
3. http://graphaware.com/neo4j/2015/01/16/neo4j-graph-model-design-labels-versus-indexed-properties.html
4. http://www.codexpedia.com/javascript/increasing-the-memory-limit-in-node-js/
5. http://stackoverflow.com/questions/2100907/how-to-remove-delete-a-large-file-from-commit-history-in-git-repository
6. http://neo4j.com/docs/2.3.2/cypher-refcard/
7. https://www.google.ie/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=neo4j%20http%20rest
8. http://neo4j.com/developer/javascript/
9. http://stackoverflow.com/questions/15161221/neo4j-script-file-format-is-there-any
10. http://stackoverflow.com/questions/21132844/neo4j-cypher-data-type-conversion
11. http://stackoverflow.com/questions/23336802/creating-nodes-and-relationships-at-the-same-time-in-neo4j
12. http://stackoverflow.com/questions/22337428/how-to-create-multiple-nodes-with-cypher-in-neo4j
13. https://dzone.com/articles/10-caveats-neo4j-users-should
14. http://stackoverflow.com/questions/15161221/neo4j-script-file-format-is-there-any
15. http://www.markhneedham.com/blog/2015/07/28/neo4j-mergeing-on-super-nodes/