/*
    Generates (Candidates), (Facebook), (Twitter) and (LinkedIn) nodes.
    Run this node program to populate the database with Candidates and Social Network Nodes. 
    
    The candidate nodes includes (where available) first name, last name, gender, photo url, 
    party profile url, website url and an [IS_ON] relationship with whatever social networks 
    they are on, each represented as its own node. 
    
    Method calls to start program are at end of file.
*/

// Module imports
var request = require("request")
var http = require('http');
var Twitter = require('twitter');
var jsonfile = require('jsonfile')
var util = require('util')

var twitCount = 0;
var twitMin;
var readCount = 0;

// Neo4J Connection info
var dbEndPoint = "localhost:7474/db/data/transaction/commit";
var username = 'neo4j', password = '12345Jm';
var neoURL = 'http://' + username + ':' + password + '@' + dbEndPoint;

// Twitter OAuth info
var client = new Twitter({
  consumer_key: 'y5fYwHYntEZytY7CMYWfaVVVs',
  consumer_secret: 'SOtuM3Ivm9BID67alybyipCjK4slQKE5u1rbR6wkOe2jOyqfwj',
  access_token_key: '468711774-U6IOeHRdgDthdiiIeEla3XCvIeaDgFInYgVGjJPG',
  access_token_secret: 'PRBgvQOwXcqga6f4yghmKHNvmoGiOKwaQANNNdsLPS0u7'
});

// Generic Neo4J HTTP callback function
var cb = function(err,data) { 
    if (data.errors.length > 0){
        // console.log(JSON.stringify(data)); 
    }
}

// Neo4J REST API POST function with callback
function cypher(neoURL,query,cb) {
    request.post({uri:neoURL,
         json:{statements:[{statement:query}]}},
         function(err,res) { cb(err,res.body)})
}

// Neo4J REST API POST function without callback
function cypher2(neoURL,query) {
    request.post({uri:neoURL,
         json:{statements:[{statement:query}]}},
         function(err,res) { })
}

// Increments counter for twitter
function incrementTwitMin() {
    var count;
    var file = __dirname+'/twitcount.json'
    jsonfile.readFile(file, function(err, obj) {
      twitMin = obj.count;
      var obj2 = {count: obj.count+14}
        jsonfile.writeFile(file, obj2, function (err) {
          console.log(obj.count);
        })
    })
}

function start(){
    // Builds candidates and social nodes and relationships
    // URL For Irish Elections OPEN API Candidates information
    var urlAPI = "http://irish-elections.storyful.com/candidates.json";
            request({
            url: urlAPI,
            json: true
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                neoCreate(body, constituencyNodesBuilder);
            }
        })
}

// Builds constituencies and relationships to candidates
function constituencyNodesBuilder(){
    // URL For Irish Elections OPEN API constituencies information
    var urlAPI = "http://irish-elections.storyful.com/constituencies.json";

    // Requests and returns the constituencies json object 
    request({
        url: urlAPI,
        json: true
    }, function (error, response, body) {

    if (!error && response.statusCode === 200) {
        var constituency = '';
        for(var i = 0; i < body.constituencies.length; i++){
            constituency = body.constituencies[i].slug;
            var query= "CREATE (constituency:Constituency { area: '" +constituency+ "'})";
            cypher2(neoURL, query);
        }

        for(var j = 0; j < body.constituencies.length; j++){
            constituency = body.constituencies[j].slug;
            var rel = "MATCH (candidate:Candidate { constituency : '" +constituency+ "'}), (constituency:Constituency{area : '" +constituency+ "'}) CREATE (candidate)-[r:IS_RUNNING_IN]->(constituency)";
            cypher2(neoURL, rel);
        }
    }
    })  
}

// Iterates through the candidates json object. Builds and sends Neo4J queries for each candidate. 
function neoCreate(election, callback){
    twitCount = 0;
    // For each candidate run query builder function
    for(var i = 0; i < election.candidates.length; i++){
        candidateQueryBuilder(election.candidates[i]);
        console.log('Created candidate ' + i);
    }
    
    // Make sure the callback is a function​
    if (typeof callback === "function") {
    // Execute the callback function
    callback();
    }
};

// Query builder function for candidates
function candidateQueryBuilder(candidate){
    // Normalise name, by removing singlequotes from within some last names.
    var firstName = candidate.first_name.replace("'", "");
    var lastName = candidate.last_name.replace("'", "");
    var partyName = candidate.party.name.replace(/á/g,"a").replace(/ó/g,"o").replace(/ú/g,"u").replace(/í/g,"i").replace(/é/g,"e").replace(/[\W_]+/g,"_");
    // var constituency = candidate.constituency.slug;
    
    // Create the candidate node cypher query string beginNing  
    var query= "CREATE (candidate:Candidate:"+partyName+" { first_name : '" +firstName+ "', last_name : '" +lastName+ "'";
     
    // Append the gender if available to the cypher query string
    if(candidate.gender !== null && candidate.gender !== ""){
        query += ", gender : '" +candidate.gender+ "'"; 
    }
    
    // Append the photo_url if available to the cypher query string
    if(candidate.photo_url !== null && candidate.photo_url !== ""){
        query += ", photo_url : '" +candidate.photo_url+ "'"; 
    }
    
    // Append the party_profile_url if available to the cypher query string
    if(candidate.party_profile_url !== null && candidate.party_profile_url !== ""){
        query += ", party_profile_url : '" +candidate.party_profile_url+ "'"; 
    }
    
    // Append the website_url if available to the cypher query string
    if(candidate.website_url !== null && candidate.website_url !== ""){
        query += ", website : '" +candidate.website_url+ "'"; 
    }
    
    // Append the website_url if available to the cypher query string
    if(candidate.phone_1 !== null && candidate.phone_1 !== ""){
        query += ", phone : '" +candidate.phone_1+ "'"; 
    }
    
    try {
        // Append the constituency if available to the cypher query string
        if(candidate.constituency.slug !== null && candidate.constituency.slug !== ""){
            query += ", constituency : '" +candidate.constituency.slug+ "'"; 
        }
    } catch (error) {
        // DEBUG: console.log(error);
        console.log("No constituency found for that candidate")
    }
    
    // Finish the query string by adding brackets
    query += "})";
    
    // POST query to database
    cypher2(neoURL, query);

    socialQueryBuilder(candidate.twitter_url, "Twitter");
    socialQueryBuilder(candidate.facebook_url, "Facebook");
    socialQueryBuilder(candidate.linkedin_url, "LinkedIn");
    
    function socialQueryBuilder(url, network){
        // If candidates [social network] url is available CREATE [social network] node and relationship with the candidate.
        if(url !== null && url !== ""){
            // Remove unwanted singlequoted from the URL
            var urlClean = url.replace("'", "");
            var query;
             var twitterHandle;
            // Scapes twitter every 20 minutes to get influencers
            // if(network == "Twitter"){
            //     console.log('min: '+ twitMin + '; count: '+ twitCount);
            //     var twitterHandle = urlClean.replace('https://twitter.com/','');
            //     var params = {screen_name: 'nodejs'};
            //     twitCount++;
            //     // Time bucket = 15
            //     if(twitCount > twitMin){
            //         client.get('https://api.twitter.com/1.1/friends/ids.json?cursor=-1&amp;screen_name='+ twitterHandle +'&amp;count=5000', params, function(error, tweets, response){
            //           if (!error) { 
            //             var file = __dirname+'/data/'+twitterHandle+'.json';
            //             var obj = tweets;
                         
            //             jsonfile.writeFile(file, obj, function (err) {
            //             console.error(err)
            //             })
            //           }else{
            //                 console.log(error);
            //             }
            //         });
            //     }   
            // }
            
            if(network == "Twitter"){
                twitterHandle = urlClean.replace('https://twitter.com/','');
                // var file = __dirname+'/data/'+twitterHandle+'.json';
                // try {
                // //    jsonfile.readFileSync(file); 
                // //    console.log(file);
                // jsonfile.readFile(file, function(err, obj) {
                //     if(obj !== undefined){
                //         var props = {
                //             "props" : [{
                //                 "id" : obj.ids[1]
                //             },{
                //                 "id" : obj.ids[2]
                //             }]
                //         }
                //         // for(i = 1; i < obj.ids.length; i++){
                //             // console.dir('id ' + obj.ids[i]);
                //         var twitLeafQuery = "CREATE (n:TwitterLeaf { "+ props +" })";
                //         cypher(neoURL, twitLeafQuery, cb);
                //         // }
                //     }                    
                // })
                // } catch (error) {
                //     console.log("no twitter");
                // }
                
            }
                 
            query = "CREATE ("+network+":"+network+" { url : '"  +urlClean+ "', handle: '"+ twitterHandle +"'})";

            
            // cbRelationship callback handles creating relationship between [social network] node and candidate node
            var cbRelationship = function(err,data) { 
                // Default error handling
                if (data.errors.length > 0){
                    console.log(JSON.stringify(data)); 
                }
                // MATCH node on first name and last name of current candidate and CREATE a [IS_ON] relationship between it and the new [social network] node (cyher query string)
                var rel = "MATCH (candidate:Candidate { first_name : '" +firstName+ "', last_name : '" +lastName+ "'}), ("+network+":"+network+"{url : '" +urlClean+ "'}) CREATE (candidate)-[r:IS_ON]->("+network+")";
                
                // POST query string and CREATE relationship
                cypher2(neoURL, rel);
            }
            // POST query string and CREATE node
            cypher(neoURL, query, cbRelationship);    
        }
    }
}

// Start Application with these method calls
incrementTwitMin();
start();