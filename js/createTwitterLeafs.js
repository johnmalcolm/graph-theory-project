 // Module imports
var request = require("request")
var http = require('http');
var Twitter = require('twitter');
var jsonfile = require('jsonfile')
var util = require('util')
var fs = require('fs');

var twitCount = 0;
var twitMin;
var readCount = 0;
var twitIncrementer = 0;

// Neo4J Connection info
var dbEndPoint = "localhost:7474/db/data/transaction/commit";
var username = 'neo4j', password = '12345Jm';
var neoURL = 'http://' + username + ':' + password + '@' + dbEndPoint;

// Generic Neo4J HTTP callback function
var cb = function(err,data) { 
    if (data.errors.length > 0){
        console.log(JSON.stringify(data)); 
    }
}

// Neo4J REST API POST function
function cypher(query,params, cb, twitterHandle) {
  request.post({uri:neoURL,
          json:{statements:[{statement:query,parameters:params}]}},
         function(err,res) { cb(err,res.body, twitterHandle)})
}  

// Neo4J REST API POST function
function cypher2(query) {
  request.post({uri:neoURL,
         json:{statements:[{statement:query}]}},
         function(err,res) { })
}        
                        
                 
 /*
 Reads local data files and sends queries to Neo4J for building Twitter Leaf nodes.
 */                   
function readFiles(dirname) {
    console.log(dirname);
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
    //   onError(err);
      return;
    }
    filenames.forEach(function(filename) {
        var file = dirname+filename;
        jsonfile.readFile(file, function(err, obj) {
            if(obj !== undefined){
                var props = {"props":[]};
                var twitterHandle = filename.slice(0, -5);
                for(i = 0; i < obj.ids.length; i++ ){
                    props.props.push({"twitter_id" : obj.ids[i], "followed_by": twitterHandle});
                }
                
                callBackRel = function(err, data, twitterHandle){
                    var query2 = "CREATE INDEX ON :TwitterLeaf(twitter_id)";
                    var query = "MATCH (a:Twitter),(b:TwitterLeaf) WHERE a.handle = '"+ twitterHandle +"' AND b.followed_by = '"+ twitterHandle +"' CREATE (a)-[r:friends]->(b)";
                    cypher2(query2);
                    cypher2(query);
                }
                
                cypher("UNWIND { props } AS map CREATE (n:TwitterLeaf) SET n = map", props, callBackRel, twitterHandle);

        } 
        });               
    });
  });
  
}

for (i = 1; i < 18; i++){
    readFiles(__dirname+'/data/'+ i +'/');
}

