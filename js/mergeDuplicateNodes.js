var http = require('http');
var request = require("request")

// Neo4J Connection info
var dbEndPoint = "localhost:7474/db/data/transaction/commit";
var username = 'neo4j', password = '12345Jm';
var neoURL = 'http://' + username + ':' + password + '@' + dbEndPoint;

// Generic Neo4J HTTP callback function
var cb = function(err,data) { 
    if (data.errors.length > 0){
        console.log(JSON.stringify(data)); 
    }
}

// Neo4J REST API POST function
function cypher(query) {
    request.post({uri:neoURL,
         json:{statements:[{statement:query}]}},
         function(err,res) { })
}

// var query = "MATCH (c:TwitterLeaf) WITH c.twitter_id as name, collect(c) as accounts, count(*) as cnt WHERE cnt > 1 WITH head(accounts) as first, tail(accounts) as rest LIMIT 1000 UNWIND rest AS to_delete MATCH (to_delete)<-[r:FRIENDS]-(e:Twitter) MERGE (first)<-[:FRIENDS]-(e) DELETE r DELETE to_delete RETURN count(*)";

// var query = "START first=node(TwitterLeaf), second=node(TwitterLeaf)WHERE has(first.twitter_id) and has(second.twitter_id) WITH first, second WHERE first.twitter_id= second.twitter_id SET first=second";
var query = "MERGE (a:TwitterLeaf) WITH (b:TwitterLeaf) WHERE a.twitter_id = b.twitter_id";

cypher(query);